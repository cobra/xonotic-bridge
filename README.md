# xonotic-bridge

A Matrix\<-\>Xonotic relay using `matrix-nio` and `xrcon`

# Setup

First, set up the virtualenv and install the dependencies:

```
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
```

Then, edit the config to fit your use-case.

```
cp config.py.example config.py
$EDITOR config.py
```

Finally, run the script:

```
python3 xonotic-bridge.py
```

# What is `./logpipe`?

Because Xonotic servers output everything indiscriminately, I have decided to leave it up to the user to somehow get the messages they want to be sent to the `./logpipe` FIFO.
